/***************************************************************************
 *   Copyright (C) 2006 by Mikko Vainio                                    *
 *   mivainio@abo.fi                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <string>
#include <sys/stat.h>
#include <fstream>
#include <map>

#include <boost/program_options.hpp> // from www.boost.org
#include <boost/tokenizer.hpp>

using namespace std;
namespace po = boost::program_options;

/** Ask user to prompt either yes or no or y or n until he/she does.
  	First print your question and then call this function.
  	<code>#include <string></code>
  	@return True if answer was yes, false if anwer was no.
*/
bool readYesNoAnswer()
{
	bool answer_ok = false;
	string answer;
	do
	{
		cin >> ws >> answer;
		transform( answer.begin(), answer.end(), answer.begin(), ::tolower );
		if(answer.compare("yes") == 0 || answer.compare("y") == 0 ) return true;
		else if(answer.compare("no") == 0 || answer.compare("n") == 0 ) return false;
		else cout << "Answer either 'y' or 'n' or 'yes' or 'no'.\n"
			<< "Well, how is it? ";
	}
	while(!answer_ok);

	return false;
}

/** Check if file exists and ask for overwrite if it does.
	Returns false if file does not exist or if user answers
	"YES".
	@param	_filename	The name of the file.
	@return	False if the file does not exist or if user answers "yes" or "y"
			to the overwrite prompt.
*/
bool askForOverwrite(const char* _filename)
{
	string filename(_filename);
	if(filename.empty()) return false;

	struct stat info;

	if(stat(_filename,&info) != 0) return false;

	if(info.st_size > 0)
	{
		cout << "\nFile " << filename << " exists. Overwrite (yes|[no]) ? ";
		return !readYesNoAnswer();
	}
	return false;
}

/** Fetch the line from stream and dump it in string.
	Handles the end of lines of different platforms.
	@return bool true if successful.
	@author Kurt Kokko, adapted by Mikko Vainio
*/
bool GetTheLine(string & lineOfText, istream & inFileStream )
{
	bool rvalue = false;			// return value: true or false
	char ch		= 0;				// "Clear" the char

	lineOfText.erase();			// Clear the string

	do
	{
		// Peek to find an EOF (This is a problem!!)
		if( (ch = inFileStream.peek()) == EOF )
		{
			rvalue = false;
			break;
		}

		ch = inFileStream.get();	// Get the character

		if( ch >= 32 )				// If the ch >= ' ' (space)
		{
			lineOfText += static_cast<char>(ch);
		} else if( ch == 13 )			// If we find the line end (UNIX): (13 or '\x0A' or '\r')
		{
			ch = inFileStream.peek();	// Peek the next character

			if( ch == 10 )				// If we find newline ('\n')
				ch = inFileStream.get();

			//lineOfText = Line;	// ... else we have apples in box and we are at the end

			rvalue = true;
			break;
		} else if( ch == 10 )			// If we find the line end (10 or '\x0D' or '\n')
		{
			ch = inFileStream.peek();	// Peek the next character
			if( ch == 13 )				// If the next character is carriage return (13 or '\x0A' or '\r')
				ch = inFileStream.get();	// get it out (we have MSDOS-type end)
			//lineOfText = Line;		// ... else we have apples in box and we are at the end
			rvalue = true;
			break;
		}
		else
		{
			lineOfText += static_cast<char>(ch);
		}
	}
	while( !inFileStream.fail() );

	if(!lineOfText.empty())
		rvalue = true;
	return rvalue;
}

const int cherryMajorVersion = 0;
const int cherryMinorVersion = 1;
const int cherrySubminorVersion = 1;

int main(int argc, char *argv[])
{
	cout << "\nSDFCherry - pick molecules from an SDF file.\n"
	<< "version " << cherryMajorVersion << "." << cherryMinorVersion << "." << cherrySubminorVersion
	<< " build " << __DATE__ << " " << __TIME__
	<< "\nCopyright (C) 2006, 2007 Mikko Vainio.\n"
	<< "This is free software; see the source for copying conditions.  There is NO\n"
	<< "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n" << endl;

	ostringstream usage;
	usage << argv[0] << " [options] <tag file> <input file> [<input file> ... ] <output file>" << endl;
	
	string infilename, outFileName, tagFileName, configFileName, addFieldName;
	bool verbose = false, invert = false, rename = false, unique = false;
	vector< string > inFileNameVec;
	po::options_description optdesc("Options");
	optdesc.add_options()
		("help,h", "Print a help message.")
		("verbose,v", "Be verbose.")
		("config", po::value< string >( &configFileName ),
			"The name of a file to read for default options. Command-line options"
			" override the ones in the config file. A config file may contain lines that look like"
			"\n'long_option_name = value'\nand comment lines that begin with '#'." )
		("tag-file,p", po::value<string>(&tagFileName),
			"Name for a file containing the tags that are matched against the molecules. "
			" The tag string may not contain tabulator characters. The file should contain"
			" one tag string per line. If the 'addfield' option is used, each line should"
			" contain an additional string that will be "
			" added as a data field to the molecule(s) that match the tag.")
		("rename,r", "Rename the molecules using the tag as the new name. Note that there might be multiple hits"
		 " by the same tag, resulting in non-unique molecule names.")
		("addfield,a", po::value<string>(&addFieldName),
			"Add a data field with the given name into the SD file. The value for each molecule is provided as a second"
			" string on each line of the tag file. Useful for adding, for example, biological activity data to SDF file "
			" for performing quantitative structure-activity (QSAR) studies.")
		("input-file,i", po::value< vector<string> >(&inFileNameVec), "Input file name. May occur multiple times."
			" If no output file is provided, the last name in the input file list is used as the output file.")
		("output-file,o", po::value<string>(&outFileName),
			"Name for the output file. If not given,"
			" the last filename in the input list is taken as the output file name." )
		("invert", "Invert the match, i.e. output molecules that do NOT match any of the tags. No data fields can be added when using invert match." )
		("unique,u", "Match each tag only once.")
	;

	po::positional_options_description popt;
	popt.add("tag-file",1);
	popt.add("input-file", -1);
	po::variables_map varmap;
	usage << optdesc << endl;
	try
	{
		po::store(po::command_line_parser(argc, argv).options(optdesc).positional(popt).run(), varmap);
		po::notify( varmap );
	}
	catch(exception& e)
	{
		// probably an unknown option
		cerr << "ERROR: " << e.what() << endl;
		cout << usage.str() << endl;
		return(EXIT_FAILURE);
	}
	catch(...)
	{
		cerr << "ERROR: Exception of unknown type!\n" << endl;
		return(EXIT_FAILURE);
	}
	if (varmap.count("help"))
	{
		cout << usage.str() << endl;
		return(EXIT_SUCCESS);
	}
	if( varmap.count("config") )
	{
		cout << "Trying to read options from " << configFileName << "...";
		cout.flush();
		ifstream ifs( configFileName.c_str() );
		if( !ifs.is_open() )
			cout << "no such file." << endl;
		else
		{
			po::store( po::parse_config_file( ifs, optdesc ), varmap );
			po::notify( varmap );
			cout << "done." << endl;
		}
	}
	if( !varmap.count("tag-file") )
	{
		cerr << "No tag file specified!" << endl;
		return(EXIT_FAILURE);
	}
	if( !varmap.count("input-file") )
	{
		cerr << "No input specified!" << endl;
		return(EXIT_FAILURE);
	}
	else if( inFileNameVec.size() > 1 && outFileName.empty() )
	{
		// take the last argument as output file
		outFileName = inFileNameVec.back();
		inFileNameVec.pop_back();
	}
	if( outFileName.empty() )
	{
		cerr << "No output file specified!" << endl;
		return(EXIT_FAILURE);
	}
	if( varmap.count("verbose") )
	{
		verbose = true;
		cout << "Being verbose." << endl;
	}
	if( varmap.count("invert") )
	{
		invert = true;
		if( verbose )
			cout << "Invert match." << endl;
	}
	if( varmap.count("addFieldName") )
	{
		if( invert )
		{
			cerr << "Error: 'addfield' contradicts with 'invert'." << endl;
			return EXIT_FAILURE;
		}
		if( addFieldName.find_first_of( "<>" ) != string::npos )
		{
			cerr << "Error: The value of 'addfield' may not contain characters '<>'." << endl;
			return EXIT_FAILURE;
		}
		if( verbose )
			cout << "Adding a data field '" << addFieldName << "' to each molecule that matches." << endl;
	}
	if( varmap.count("rename") )
	{
		rename = true;
		if( invert )
		{
			cerr << "Error: 'rename' contradicts with 'invert'." << endl;
			return EXIT_FAILURE;
		}
		if( verbose )
			cout << "The matched molecules will be renamed using the tag as the new name." << endl;
	}
	if( varmap.count("unique") )
	{
		unique = true;
		if( invert )
		{
			cerr << "Error: 'unique' contradicts with 'invert'." << endl;
			return EXIT_FAILURE;
		}
		if( verbose )
			cout << "The molecules will be matched only once. The first match is output, subsequent matches are ignored." << endl;
	}

	if( askForOverwrite( outFileName.c_str() ) )
	{
		cout << "Abort." << endl;
		return EXIT_SUCCESS;
	}
	// open the output file
	ofstream out;
	out.open( outFileName.c_str(), ios::binary );
	if( !out )
	{
		cout << "Error: Could not open the output file '" << outFileName << "'. Abort." << endl;
		return EXIT_FAILURE;
	}

	ifstream in;

	// open the tag file
	in.open( tagFileName.c_str(), ios::binary );
	if( !in )
	{
		cout << "Error: Could not open the tag file '" << tagFileName << "'. Abort." << endl;
		return EXIT_FAILURE;
	}
	// read the tag strings
	map< string, pair< unsigned, string > > tags; // key: tag string, value: hit counter and (optional) value for the data field to add
	boost::char_separator<char> separators("\t"); // use tab to separate fields
	boost::tokenizer< boost::char_separator<char> >::const_iterator tokenIter;
	do
	{
		string tmp, tag;
		GetTheLine( tmp, in );
		if( tmp.empty() ) continue;
		boost::tokenizer< boost::char_separator<char> > tok( tmp, separators );
		tokenIter = tok.begin();
		tag = *(tokenIter++);
		pair< unsigned, string > pari = make_pair( 0, ( tokenIter != tok.end() ? *tokenIter : "" ) );
		tags[ tag ] = pari;
	}
	while( !in.fail() && in.peek() != EOF );
	if( tags.empty() )
	{
		cout << "Error: No tags read in from '" << tagFileName << "'. Abort." << endl;
		return EXIT_FAILURE;
	}
	if( verbose )
		cout << "Got " << tags.size() << " tag strings." << endl;
	in.close();
	in.clear();
	int nmolecules = 0, nmatches = 0;
	// process the input files
	vector< string > filecontent; // temporary storage for the latest molecule read from sdf.
	for( int i = 0; i < inFileNameVec.size(); ++i )
	{
		cout << "\n";
		if( tags.empty() ) break;
		infilename = inFileNameVec.at(i);
		in.clear(); // clear failbit and eofbit
		in.open( infilename.c_str(), ios::binary );
		if( !in )
		{
			cout << "Warning: Could not open the input file '" << infilename << "'. Skipping." << endl;
			continue;
		}
		// read the file and compare with the tag strings
		bool writeout = false;
		filecontent.clear();
		string tmp, fieldValue, newName;
		do
		{
			tmp.clear();
			GetTheLine( tmp, in );

			if( !tmp.empty() && !writeout )
			{
				// compare the line to the tag strings
				for( map< string, pair< unsigned, string > >::iterator pat = tags.begin();
					pat != tags.end() && !writeout; ++pat )
					if( tmp.find( pat->first ) != string::npos )
					{
						++(pat->second.first); // increase hit counter
						fieldValue = pat->second.second; // the value of the data field to add
						newName = pat->first; // new name
						writeout = true; // also breaks the for-loop
						if( unique )
							tags.erase( pat->first );
					}
			}

			filecontent.push_back( tmp ); // also '$$$$' ends up in filecontent

			if( !tmp.empty() && tmp.find("$$$$") != string::npos )
			{
				// a new molecule starts. write out the old (if any).
				++nmolecules;
				if( invert && !filecontent.empty() ) writeout = !writeout;
				if( writeout )
				{
					++nmatches;
					if( rename )
						filecontent.at(0) = newName; // replace the old name with the found tag
					if( !addFieldName.empty() )
						// insert the data field berfore the '$$$$' on the last line
						filecontent.back() = "> <" + addFieldName + ">\n" + fieldValue + "\n\n$$$$";
					for( vector< string >::iterator sitr = filecontent.begin(); sitr != filecontent.end(); ++sitr )
						out << *sitr << "\n";
				}
				writeout = false;
				filecontent.clear();
				fieldValue.clear();
				newName.clear();
				// print status for the convenicence(?) of the user
				// after every 50th molecule
				if( 0 == nmolecules%50 )
				{
					cout << "\r" << infilename << ": " << setw(8) << nmatches << " matches out of " << setw(10) << nmolecules << " processed molecules.";
					cout.flush();
				}
			}

		}
		while( (!tags.empty() || writeout) && !in.fail() && in.peek() != EOF );

		in.close();
		in.clear();
	}
	// print the final status
	cout << "\r" << infilename << ": " << setw(8) << nmatches << " matches out of " << setw(10) << nmolecules << " processed molecules." << endl;
	out.close();

	// write out tags with zero hits
	unsigned nmissed = 0;
	for( map< string, pair< unsigned, string > >::iterator pat = tags.begin(); pat != tags.end(); ++pat )
	{
		if( 0 == pat->second.first ) // zero hit count
		{
			if( 0 == nmissed ) // write out a header line
				cout << ( invert ? "Inverse hits" : "Missed tags:" ) << "\n";
			cout << pat->first << "\n";
			++nmissed;
		}
	}
	if( 0 < nmissed )
		cout << "\n" <<  ( invert ? "Inverse hit" : "Missed" ) << " a total of " << nmissed << " tags." << endl;

	return EXIT_SUCCESS;
}
